import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:healy/collections/teammember_collection.dart';
import 'package:healy/models/teammember_model.dart';
import 'package:healy/repositories/teammember_repository.dart';
import 'package:healy/widgets/layout/appbar_with_heading.dart';
import 'package:healy/widgets/layout/indicator.dart' as _;
import 'package:healy/widgets/layout/smartrefresher/footer.dart';
import 'package:healy/widgets/layout/snack_box.dart';
import 'package:healy/widgets/team/team_empty.dart';
import 'package:healy/widgets/team/team_item.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class TeamList extends StatefulWidget {
  @override
  _TeamListState createState() => _TeamListState();
}

class _TeamListState extends State<TeamList> with AfterLayoutMixin<TeamList> {
  static const int limit = 20;
  bool _isLoading;
  int _currentPage = 1;
  String _sortBy = 'id';
  String _sortType = 'DESC';
  RefreshController _refreshController;
  List<TeamModel> _teamMemberList;

  @override
  void afterFirstLayout(BuildContext context) {
    loadTeamList();
  }

  void loadTeamList() async {
    if (_currentPage == 1) {
      setState(() {
        _isLoading = true;
      });
      _refreshController.refreshCompleted();
    }
    TeamCollection collection = await TeamRepository().getItems(
      page: _currentPage,
      limit: limit,
      sortBy: _sortBy,
      sortType: _sortType,
    );
    if (collection.error == null) {
      setState(() {
        _isLoading = false;
        if (_currentPage == 1) {
          _refreshController.refreshCompleted();
          _teamMemberList = collection.items;
        } else if (_currentPage > 1) {
          for (TeamModel teamMemberModel in collection.items) {
            _teamMemberList.add(teamMemberModel);
          }
          _refreshController.loadComplete();
        }
        if (collection.items.length < limit) {
          _refreshController.loadNoData();
        }
      });
    } else {
      if (_currentPage == 1) {
        _refreshController.refreshFailed();
      }
      setState(() {
        _isLoading = false;
      });
      SnackBox.show(context, collection.error.errors);
    }
  }

  @override
  void initState() {
    super.initState();

    _isLoading = true;
    _teamMemberList = [];
    _refreshController = RefreshController();
  }

  void _onRefresh() {
    setState(() {
      //reset to first page
      _currentPage = 1;

      //allow load more
      _refreshController.resetNoData();

      //load products list again
      loadTeamList();
    });
  }

  void _onLoading() {
    setState(() {
      //reset to first page
      _currentPage++;

      //load products list again
      loadTeamList();
    });
  }

  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  Widget renderOrder() {
    Widget promotionWrapper;

    if (_teamMemberList.length > 0) {
      promotionWrapper = Scaffold(
        appBar: AppBarWithHeading.getAppBar(
          context,
          'All team (${_teamMemberList.length})',
          actions: <Widget>[
            IconButton(
              onPressed: () {
                Navigator.of(context).pushNamed('/teamcreate', arguments: null);
              },
              icon: Icon(
                Icons.add_circle,
                color: Colors.white,
              ),
            )
          ],
        ),
        body: SafeArea(
          child: ListView.separated(
            itemBuilder: (context, index) => TeamItem(_teamMemberList[index]),
            separatorBuilder: (context, index) => Divider(
              color: Colors.grey,
            ),
            itemCount: _teamMemberList.length,
          ),
        ),
      );
    } else {
      promotionWrapper = TeamEmpty();
    }

    return promotionWrapper;
  }

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      enablePullDown: true,
      enablePullUp: true,
      footer: Footer.get(context),
      header: WaterDropMaterialHeader(
        color: Colors.blue,
        backgroundColor: Colors.blue,
      ),
      controller: _refreshController,
      onRefresh: _onRefresh,
      onLoading: _onLoading,
      child: _isLoading ? _.Indicator() : renderOrder(),
    );
  }
}
